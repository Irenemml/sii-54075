# Changelog

## [5.0] - 28/12/2020
## Añadido 
-Creación de una clase para la conexión entre mundo y servidor mediante sockets

## Modificado
-MundoCliente.cpp y MundoServidor.cpp

## Eliminado
-Conexiones del mundo cliente y el mundo servidor

## [4.0] - 16/12/2020
## Añadido 
-Creación de una estructura de cliente servidor en el juego. El servidor está conectado con el logger y envía datos al cliente. EL cliente a su vez está conectado con el bot.

## Modificado
-bot.cpp y logger.cpp para que correspondan con la nueva estructura.
-Archivo CMakeLists.txt para poder generar ejecutables 

## [3.0] - 03/12/2020
## Añadido 
-logger.cpp el cual generará un ejecutable para comunicarse con mundo a través de un tubería FIFO
-bot.cpp se encarga de controlar la raqueta del jugador 1 y podrá controlar la raqueta del jugador 2 cuando este no haya pulsado ninguna tecla durante 10 segundos.
-DatosMemCompartida.h para la comunicación entre bot y Mundo
-Puntuacion.h

## Modificado
-Archivo CMakeLists.txt
-Mundo.cpp y Mundo.h para implementar las funciones de logger.cpp y bot.cpp. También se ha implementado en este archivo la finalización del programa cuando alguno de los jugadores llega a 3 puntos.
-Plano.h y Plano.cpp

## [2.0] - 18/11/2020
## Añadido
-Nuevo Changelog y borrar antiguo
-Creación de fichero README para instrucciones
-Funciones de disminución tamaño esfera 
## Modificado
-Changelog y funciones Mueve de Esfera y de Raqueta

## [1.2] - 22/10/2020
## Añadido
-Primer commit
-Changelog
## Modificado
-Cambio Ficheros de cabecera con nombre del  autor 


